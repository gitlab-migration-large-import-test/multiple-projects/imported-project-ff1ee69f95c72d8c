.cache: &cache
  key:
    prefix: $RUBY_VERSION
    files:
      - Gemfile.lock
      - gitlab-qa.gemspec
  paths:
    - vendor/ruby

include:
  - project: 'gitlab-org/quality/pipeline-common'
    file:
      - '/ci/gem-release.yml'
      - '/ci/danger-review.yml'

stages:
  - check
  - test
  - qa
  - deploy

default:
  image: ${CI_REGISTRY}/gitlab-org/gitlab-build-images/debian-${DEBIAN_VERSION}-ruby-${RUBY_VERSION}:bundler-2.3
  tags:
    - gitlab-org
  cache:
    <<: *cache
    policy: pull
  before_script:
    - bundle install

workflow:
  rules:
    # For merge requests, create a pipeline.
    - if: '$CI_MERGE_REQUEST_IID'
    # For the default branch, create a pipeline (this includes on schedules, pushes, merges, etc.).
    - if: '$CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH || $CI_COMMIT_REF_NAME =~ /^[\d-]+-stable$/'
    # For tags, create a pipeline.
    - if: '$CI_COMMIT_TAG'
    # When using Run pipeline button in the GitLab UI, from the project’s CI/CD > Pipelines section, create a pipeline.
    - if: '$CI_PIPELINE_SOURCE == "web"'

variables:
  DEBIAN_VERSION: bullseye
  RUBY_VERSION: "3.0"
  BUNDLE_PATH: vendor
  BUNDLE_SILENCE_ROOT_WARNING: "true"
  BUNDLE_SUPPRESS_INSTALL_USING_MESSAGES: "true"

# Override rules to allow creating backport releases
.release-base:
  rules:
    - if: '$CI_PIPELINE_SOURCE == "push" && ($CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH || $CI_COMMIT_REF_NAME =~ /^[\d-]+-stable$/)'
      changes: ["lib/**/version.rb"]

package-and-test-env:
  image: ${CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX}/alpine:latest
  stage: .pre
  variables:
    BUILD_ENV: build.env
  cache: []
  before_script: []
  script:
    - scripts/build-package-and-test-env
  artifacts:
    reports:
      dotenv: $BUILD_ENV
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - Gemfile.lock
      variables:
        UPDATE_QA_CACHE: "true"
    - if: '$CI_MERGE_REQUEST_IID'
      changes:
        - Gemfile.lock
      variables:
        UPDATE_QA_CACHE: "true"
    - when: always
      variables:
        UPDATE_QA_CACHE: "false"

cache-gems:
  stage: .pre
  script:
    - echo "Cached gems!"
  cache:
    <<: *cache
    policy: pull-push
    when: on_success
  rules:
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'
      changes:
        - Gemfile.lock
    - if: '$CI_MERGE_REQUEST_IID'
      changes:
        - Gemfile.lock

danger-review:
  image: ${CI_REGISTRY}/gitlab-org/gitlab-build-images/debian-bullseye-ruby-${RUBY_VERSION}:bundler-2.4
  stage: check
  needs:
    - job: cache-gems
      optional: true

rubocop:
  stage: check
  needs:
    - job: cache-gems
      optional: true
  script:
    - bundle exec rubocop --color

rspec:
  stage: test
  needs:
    - job: cache-gems
      optional: true
  coverage: /LOC \((\d{1,3}\.\d{1,2})\%\)/
  script:
    - bundle exec rspec --force-color
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: coverage/coverage.xml

package-and-test:
  stage: qa
  needs:
    - package-and-test-env
  variables:
    RELEASE: EE
    RUN_WITH_BUNDLE: "true"
    SKIP_OMNIBUS_TRIGGER: "true"
    SKIP_REPORT_IN_ISSUES: "true"
    ALLURE_JOB_NAME: gitlab-qa
    UPDATE_QA_CACHE: $UPDATE_QA_CACHE
    GITLAB_QA_CACHE_KEY: $GITLAB_QA_CACHE_KEY
  inherit:
    variables:
      - RUBY_VERSION
      - DEBIAN_VERSION
  trigger:
    strategy: depend
    forward:
      yaml_variables: true
      pipeline_variables: true
    include:
      - project: gitlab-org/gitlab
        ref: master
        file: .gitlab/ci/package-and-test/main.gitlab-ci.yml
  rules:
    - if: '$CI_MERGE_REQUEST_IID'
      when: manual
      allow_failure: true
    - when: always